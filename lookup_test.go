package smtp

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLookup(t *testing.T) {
	assert.Equal(t, "smtp.gmail.com", Lookup("gmail.com"))
	assert.Equal(t, "smtp.gmail.com", Lookup("google.com"))
	assert.Equal(t, "smtp.yandex.ru", Lookup("yandex.ru"))
	assert.Equal(t, "smtp.yandex.ru", Lookup("ntkpribor.ru"))
}
