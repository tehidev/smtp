module gitlab.com/knopkalab/go/smtp

go 1.17

require (
	github.com/jordan-wright/email v4.0.1-0.20210109023952-943e75fe5223+incompatible
	github.com/stretchr/testify v1.7.0
	gitlab.com/knopkalab/go/konfig v1.0.3
	gitlab.com/knopkalab/tracer v1.7.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/natefinch/lumberjack v2.0.0+incompatible // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rs/zerolog v1.26.1 // indirect
	gitlab.com/knopkalab/errors v1.1.0 // indirect
	gitlab.com/knopkalab/go/utils v1.0.0 // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	golang.org/x/sys v0.0.0-20220204135822-1c1b9b1eba6a // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
