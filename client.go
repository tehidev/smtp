package smtp

import (
	"context"
	"errors"
	"mime"
	"net/mail"
	"net/smtp"
	"net/textproto"
	"path/filepath"

	"github.com/jordan-wright/email"

	"gitlab.com/knopkalab/tracer"
)

// Client smtp with queue
type Client struct {
	span tracer.SpanContext
	conf *ClientConfig

	done   chan struct{}
	Client chan *email.Email
}

// NewClient smtp with queue
func NewClient(ctx context.Context, conf ClientConfig) (*Client, error) {
	if err := conf.ValidAndRepair(); err != nil {
		return nil, err
	}

	span := tracer.StartSpanContext(ctx, "smtp client")
	span.Debug().Msg("ready")

	client := &Client{
		span:   span,
		conf:   &conf,
		done:   make(chan struct{}),
		Client: make(chan *email.Email, conf.SendQueueSize),
	}

	go client.sendLoop()

	return client, nil
}

// Close send Client
func (c *Client) Close(waitForSendRemaining bool) {
	c.span.Debug().Msg("closing")
	close(c.Client)
	if waitForSendRemaining {
		c.Wait()
	}
	c.span.Debug().Msg("closed")
	c.span.Finish()
}

// Wait for send remaiming
func (c *Client) Wait() {
	<-c.done
}

func (c *Client) sendLoop() {
	for msg := range c.Client {
		span := tracer.StartSpan(c.span, "send email")

		data, err := c.send(msg)

		attachments := make([]string, len(msg.Attachments))
		for i, at := range msg.Attachments {
			attachments[i] = at.Filename
		}
		log := span.With().
			Int("size", len(data)).
			Str("title", msg.Subject).
			Str("from", msg.From).
			Strs("to", msg.To).
			Strs("bcc", msg.Bcc).
			Strs("cc", msg.Cc).
			Strs("reply_to", msg.ReplyTo).
			Strs("attachments", attachments).
			Logger()
		if err == nil {
			log.Debug().Msg("email sent")
		} else {
			log.Error().Err(err).Msg("email send error")
		}

		span.Finish()
	}
	close(c.done)
}

// Send email
func (c *Client) Send(msg *email.Email) error {
	if err := c.Prepare(msg); err != nil {
		return err
	}
	c.Client <- msg
	return nil
}

// SendNow email, without enClient
func (c *Client) SendNow(msg *email.Email) ([]byte, error) {
	if err := c.Prepare(msg); err != nil {
		return nil, err
	}
	return c.send(msg)
}

// Prepare email
func (c *Client) Prepare(msg *email.Email) error {
	if msg.From == "" {
		msg.From = c.conf.from
	}
	if len(msg.To) == 0 {
		if len(c.conf.DefaultTo) == 0 {
			return errors.New("recipients no set")
		}
		msg.To = c.conf.DefaultTo
	}
	if len(msg.Bcc) == 0 {
		msg.Bcc = c.conf.DefaultBcc
	}
	if len(msg.Cc) == 0 {
		msg.Cc = c.conf.DefaultCc
	}
	if err := c.prepareAddrs(msg.To); err != nil {
		return err
	}
	if err := c.prepareAddrs(msg.Cc); err != nil {
		return err
	}
	if err := c.prepareAddrs(msg.Bcc); err != nil {
		return err
	}

	if msg.Headers == nil {
		msg.Headers = textproto.MIMEHeader{}
	}
	for _, at := range msg.Attachments {
		if at.Header == nil {
			at.Header = textproto.MIMEHeader{}
		}
		if at.ContentType == "" {
			at.ContentType = mime.TypeByExtension(filepath.Ext(at.Filename))
		}
	}
	return nil
}

func (c *Client) prepareAddrs(addrs []string) error {
	for i, addr := range addrs {
		addr, err := mail.ParseAddress(addr)
		if err != nil {
			return err
		}
		addrs[i] = addr.Address
	}
	return nil
}

func (c *Client) send(msg *email.Email) ([]byte, error) {
	to := make([]string, 0, len(msg.To)+len(msg.Cc)+len(msg.Bcc))
	to = append(append(append(to, msg.To...), msg.Cc...), msg.Bcc...)
	sender, err := mail.ParseAddress(msg.From)
	if err != nil {
		return nil, err
	}
	data, err := msg.Bytes()
	if err != nil {
		return nil, err
	}
	return data, smtp.SendMail(c.conf.Server, c.conf.auth, sender.Address, to, data)
}
